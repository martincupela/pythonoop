#### Task

Python does not have a `Fraction` class which instances may look like : `4/5`, `6/9`, etc. We can change it
now. Implement a class `Fraction` that will provide us with the following interface:

- peform arithmentic operations with two fraction objects
- peform arithmentic operations with other numeric types
- compare fraction object with any other number
- convert fraction object into `int` or `float`


For example:

```python
>>> from fraction import Fraction
>>> num1 = Fraction(4, 5)
>>> num1
Fraction(4/5)
>>> num2 = Fraction(14, 37)
>>> num2
Fraction(14/37)
>>> num1 + num2
Fraction(218/185)
>>> num1 + 0.8
1.6
>>> num1 + 2
2.8
>>> float(num1)
0.8
>>> num1 == num2
False
>>> num1 > num2
True

```