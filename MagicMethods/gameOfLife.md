#### Task

Game of life is a famous simulation of cell life. There is a great intro into the topic provided at 
[Game of life page by Stanford University](http://web.stanford.edu/~cdebs/GameOfLife/). Once you
read it, try to implement classes that will allow you to run such a program.

You may want to implement:

- `pane`, where the cells reproduce and die
- `cell` that lives and dies based on the number of its neighbors 

You should be able draw the cells in the terminal window. Each cell can be colored using **module colorama**.

You can install the module using `pip install colorama`. Information on how to use it 
can be found at [pypi](https://pypi.org/project/colorama/).