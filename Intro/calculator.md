#### Task

Let's implement a class `Calculator` that will allow us to perform the following operations:

- arithemtic operations (+, -, /, *, etc.)
- keep the information about the last result in the memory
- clear the memory
- keep the information about the last 10 operations in the memory