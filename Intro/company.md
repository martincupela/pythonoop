#### Task

Once people are created, they need somewhere to work. For that we have something called **company**. 
Companies are made of people. Let's create  one of ours. 

The company object should be able to:

- perform selection among employee candidates
- employ people, that is to register them among its employees
- tell you, have many people are working inside
- fire people
- discriminate (e.g. based on age)
- tell you have many people they have already fired or employed (e.g. this year)


How would that look like?
