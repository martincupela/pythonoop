#### Task

Cash machine  (ATM) is a object that a normal person interacts with a lot. Here will be you task to implement 
a class representing cach machine. Objects generated by this class should allow the user to:

- deposit money
- withdraw money
- show balance
- show the history of withdrawals

