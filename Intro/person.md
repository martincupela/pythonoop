#### Task

Objects need to given birth similarly to us people. Objects are actually just an abstraction and reflection 
of the real-life stuff.
 
 Now we want to create an abstraction of a person. 
 
- What are the most important characteristics of a person?
- How can we convert them into a Python object?


The best way to answer the above questions is to determine, how do we want the person to behave and information (memories)
do we want it to keep:

```python
>>> bob.fname
'Bob'
>>> bob.lname
'Smith'
>>> bob.address
32 Main Street
>>> bob.tell_age()
32
>>> bob.echo('How are you?')
How are you?
>>> bob.guess_a_number()
11
```