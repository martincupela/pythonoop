#### Task

It is time to improve the cash machine. It should be able to accept different currency or show the client's
balance in different currencies.

For example the client could have an account in Czech Crowns, but deposit money in a differenc currency
such as American dollar.

Your task is to implement an interface for our cash machine. that will:

- contact the [fixer.io](https://fixer.io/) api if the deposited money is in different currency than the account
- retrieve the current exchange rate
- convert the money into the accounts currency and add it to the balance
- show the user the current balance