#### Task

We have a `Company` class as well as a `Person` class defined. It is time to provide the person with some skills
that will allow the company to decide, whether it wants to hire him / her.

We can imagine the skill description as a CV.

- Where will we keep the CV information?
- How will we have to adjust the `Company` class?


