#### Task

Each company employes people with various specializations. We want to define at least the most basic ones.
When declaring these jobs, let's thing about them in terms of being entities in a HR System. What each
role should be able to do in the system and what information should it have?:

##### `HRAdmin`

- belongs to a department
- has nationality
- has salary
- has performance rating
- has contract start date and end date
- can change any employee's address in the HR system
- can change any employee's deparment in the HR system
- can change any employee's salary in the HR system

##### `Programmer`

- belongs to a department
- has nationality
- has salary
- has contract start date and end date
- has performance rating
- can receive +10% increase

#####`Manage`
- belongs to a department
- has nationality
- has salary
- has contract start date and end date
- has performance rating
- has refer to department managed
- can fire employees from the department
- can approve salary increase

##### `Trainee`

- belongs to a department
- has performance rating
- does not have contract start date and end date

#### `HRManager`

- can do what `HRAdmin` and `Manager` can do

You can of course think about lot more stuff that each role can do or be in the system. 

Try to implement logic at least for those above.