#### Task

People tend to work together, because each of is able to do something different. And that is a good
example of composition in programming. Let's say we have a HR department, where we have the following
positions:

- `HRAdmin` - can change your address or deparment
- `Benefits specialist` - can tell you, whether you are eligible for increase
- `Exit specialist` - takes care of your removal from the system

Now imagine that you are a manager, that wants to interact with **HR department** as a whole and you
do not care who will do what. Let's say, the manager of deparment **LINUX** want's to fire an employee
from own department **X12352**. 

First the manager looks at how many employees are there:

```python
>>> company = Company(employees)
>>> len(company.department['LINUX'])
100
```

Then goes to HR and asks them to release the person of his / her duty:

```python
>>> hr = company.department['HR']
>>> hr.remove(id='X12312')
Error: You are not the manager
```

oops, mistake, that was not correct id - employee is not in manager's department':

```python
>>> hr.remove(id='X12352')
len(department)
9
````

Implement the classes, that will allow the manager to call the following methods on HR department:

- `remove(emp_id)`
- `change_address(emp_id, new_address)`
- `eligible(emp_id)`